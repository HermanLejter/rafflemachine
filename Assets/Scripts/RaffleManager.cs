﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaffleManager : MonoBehaviour
{

    [SerializeField] private float m_MaxX;
    [SerializeField] private float m_MinX;
    [SerializeField] private float m_MaxY;
    [SerializeField] private float m_MinY;
    [SerializeField] private GameObject m_Dot;
    [SerializeField] private float m_MinDestroyTime;
    [SerializeField] private float m_MaxDestroyTime;
    [SerializeField] private GameObject m_Camera;
    [SerializeField] private float m_CameraZoomSize;
    [SerializeField] private float m_CameraLerpSpeed;
    private GameObject m_LastDot;
    private Entry m_Winner;
    private bool m_ZoomCamera = false;
    [System.Serializable]
    public class Entry
    {
        public int weight;
        public string name;
        public Color color;
    }


    [SerializeField] private List<Entry> m_Entries = new List<Entry>();

    void Start()
    {
        m_Winner = GetWinner();
        CreateDots();
    }

    private void Update()
    {
        if (m_ZoomCamera)
        {
            Camera camera = m_Camera.GetComponent<Camera>();
            camera.orthographicSize = Mathf.Lerp(camera.orthographicSize, m_CameraZoomSize, Time.deltaTime * m_CameraLerpSpeed);
            if(Vector3.SqrMagnitude(m_Camera.transform.position - new Vector3(m_LastDot.transform.position.x, m_LastDot.transform.position.y, m_Camera.transform.position.z)) > .5f)
            {
                m_Camera.transform.position = Vector3.Lerp(m_Camera.transform.position, new Vector3(m_LastDot.transform.position.x, m_LastDot.transform.position.y, m_Camera.transform.position.z), Time.deltaTime * m_CameraLerpSpeed);
            }
            else
            {
                m_ZoomCamera = false;
                GetComponent<AudioSource>().Play();
            }
        }
    }

    public void CreateDots()
    {
        foreach (Entry entry in m_Entries)
        {
            for (int i = 0; i < entry.weight; i++)
            {
                GameObject dot = Instantiate(m_Dot, new Vector3(Random.Range(m_MinX, m_MaxX), Random.Range(m_MinY, m_MaxY), 0), Quaternion.identity);
                DotScript dotScript = dot.GetComponent<DotScript>();
                dotScript.SetupDot(entry.name, entry.color);
                // if this is the winner...
                if (entry.name == m_Winner.name)
                {
                    Debug.Log(entry.name + "Is the winner!");
                    // if this is not the last dot the winner gets...
                    if (i < entry.weight - 1)
                    {
                        dotScript.DestroyObject(Random.Range(m_MinDestroyTime, m_MaxDestroyTime));
                    }
                }
                else
                {
                    // if this is not the winner, destroy all of them
                    dotScript.DestroyObject(Random.Range(m_MinDestroyTime, m_MaxDestroyTime));
                }
            }
        }
        Invoke("CameraZoom", m_MaxDestroyTime);
    }

    public Entry GetWinner()
    {
        int totalWeight = 0;

        for (int i = 0; i < m_Entries.Count; i++)
        {
            totalWeight += m_Entries[i].weight;
        }

        int randomWeight = Random.Range(1, totalWeight + 1);
        int currentWeight = 0;

        foreach (Entry entry in m_Entries)
        {
            currentWeight += entry.weight;
            if (randomWeight <= currentWeight)
            {
                Debug.Log(entry.name);
                return entry;
            }
        }
        return null;
    }

    public void CameraZoom()
    {
        m_LastDot = FindObjectOfType<DotScript>().gameObject;
        m_ZoomCamera = true;
    }



}
