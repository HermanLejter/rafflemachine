﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DotScript : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_NameText;
    [SerializeField] private GameObject m_Particles;

    public void SetupDot(string aName, Color aColor)
    {
        m_NameText.text = aName;
        GetComponent<SpriteRenderer>().color = aColor;
    }

    public void DestroyObject(float aTime)
    {
        Destroy(gameObject, aTime);
        Invoke("CreateParticles", aTime-.01f);
    }
    public void CreateParticles()
    {
        GameObject particles =Instantiate(m_Particles, transform.position, Quaternion.identity);
        AudioSource audioSource = particles.GetComponent<AudioSource>();
        audioSource.pitch = Random.Range(.5f, 1.75f);
    }
}
